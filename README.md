


To use messages, keys and other special characters use url encoding or decoding

Service: 
Task - captures sensor data from the activity that receives it(sensor data). The sensor data is in the form of a parcel

Client:
Task: - Uses retrofit library that facilitates the "POST" of sensor data from device to the processing servers

The received message is concatenated on "~". 
The Key : Android unique id
The message concatenated on  "~" consists of two parts
The part before the "~" is the uuid(identifier for the type of the sensor for e.g. Accelerometer, Temperature sensor etc. This identfier can be looked up according to the Ti manual

The second part after the "~" denotes the actual value of the sensor. The Point 3d class provides a suitable parsing  mechanism for understanding the values which are generated as hex or binary.


 /**************************************************************************************************
DISCLAIMER.

  THIS SOFTWARE IS PROVIDED BY TI AND TIŐS LICENSORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  IN NO EVENT SHALL TI AND TIŐS LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.


 **************************************************************************************************/

package com.example.ti.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.example.ti.api.KafkaMessage;
import com.example.ti.ble.sensortag.DeviceActivity;
import com.example.ti.client.StreamClient;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by sushrut on 10/19/14.
 */
public class StreamService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * .
     */
    public StreamService() {
        super("StreamService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        /*
        take the extra written/read data out of
        the intent and does a post on the uri
        */
        KafkaMessage to_Stream= intent.getParcelableExtra(DeviceActivity.STREAM_MESSAGE);
        String concat_Kafka=to_Stream.getSensorType()+"~"+to_Stream.getSensorData();
        //Log.d("KAFKA_STREAMS",concat_Kafka);
        Map<String, String> Qmap = new LinkedHashMap<String, String>();
       //Log.d("KAFKA_STREAMS",Qmap.get("message"));
        Qmap.put("topic", "Streams"); Qmap.put("message",concat_Kafka); Qmap.put("key",to_Stream.getKey());
        StreamClient.getStreamClientEndpoint().streamData(Qmap, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                Log.d("KAFKA_STREAMS", "Success?");
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d("KAFKA_STREAMS", URLDecoder.decode(error.getUrl().toString(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
        Log.d("KAFKA_SERVICE", concat_Kafka);
    }
}


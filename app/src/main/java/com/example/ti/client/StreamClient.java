

 /**************************************************************************************************
DISCLAIMER.

  THIS SOFTWARE IS PROVIDED BY TI AND TIŐS LICENSORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  IN NO EVENT SHALL TI AND TIŐS LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.


 **************************************************************************************************/
package com.example.ti.client;

import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.POST;
import retrofit.http.QueryMap;

/**
 * Created by sushrut on 10/19/14.
 */
public class StreamClient {
    /*
    This class describes an interface using the retrofit library that acts as a rest client to POST the data received
    This class is used by the service (not Restful service) android service which has a broadcast receiver to intercept bluetooth
    sensor event changes
    By client here we mean the http client that makes the communication between the outside world(the endpoint) and the android system possible

     */
    private static StreamToBoxInterface SensorMessageStream;
    public static StreamToBoxInterface getStreamClientEndpoint(){
       // Log.d("KAFKA_STREAMS", "Trying to send if endpoint correct");
        if (SensorMessageStream==null){
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("url")
                    .build();
            SensorMessageStream=restAdapter.create(StreamToBoxInterface.class);
        }
        return SensorMessageStream;
    }

    public interface StreamToBoxInterface{


        @POST("/message")

        void streamData(@QueryMap Map<String,String> filters , Callback<Object> returnmessage);

    }
}

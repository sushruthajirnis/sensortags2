

 /**************************************************************************************************
DISCLAIMER.

  THIS SOFTWARE IS PROVIDED BY TI AND TIŐS LICENSORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  IN NO EVENT SHALL TI AND TIŐS LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.


 **************************************************************************************************/


package com.example.ti.api;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sushrut on 10/17/14.
 */
public class KafkaMessage implements Parcelable {
    String key;

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public String getSensorData() {
        return sensorData;
    }

    public void setSensorData(String sensorData) {
        this.sensorData = sensorData;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    String sensorType;
    String sensorData;

    public KafkaMessage(String key, String sensorType, String sensorData) {
        this.key = key;
        this.sensorType = sensorType;
        this.sensorData = sensorData;
    }

    protected KafkaMessage(Parcel in) {
        key = in.readString();
        sensorType = in.readString();
        sensorData = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeString(sensorType);
        dest.writeString(sensorData);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<KafkaMessage> CREATOR = new Parcelable.Creator<KafkaMessage>() {
        @Override
        public KafkaMessage createFromParcel(Parcel in) {
            return new KafkaMessage(in);
        }

        @Override
        public KafkaMessage[] newArray(int size) {
            return new KafkaMessage[size];
        }
    };
}
